# Angular 2 Seed

Angular 2 Starter Pack.

## Installation

* Clone Repository
```
#!bash

git clone https://mihaicracan@bitbucket.org/agvision/angular_seed.git
```

* Install Node Packages

```
#!bash

npm install
```

* Run Postinstall Scripts

```
#!bash

npm run postinstall
```

## How to use it

* In order to run typescript and gulp tasks and start a browser session

```
#!bash

npm start
```

* In order to build your app for production run

```
#!bash

npm run build
```